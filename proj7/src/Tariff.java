

public enum Tariff {

    WEATHER (12),
    LOVE (25),
    MUSIC (10),
    BEZLIMIT (50);
    private final int price;
    public static final double perMinute = 1.2;
    Tariff(int price) {
        this.price = price;
    }


    public int getPrice() {
        return price;
    }
}