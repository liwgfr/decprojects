import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Scanner;

public class Abonent {
    private double balance;
    private String number;
    private final ArrayList<Tariff> ar = new ArrayList<>();
    private final ArrayList<Integer> price = new ArrayList<>();
    private int counter = 0;

    public Abonent(String number, double balance) {
        this.number = number;
        this.balance = balance;
    }

    private static final Scanner in = new Scanner(System.in);
    private HashMap<String, Boolean> map = new HashMap<>();

    // Хотел сделать звонок в milliSeconds, но потом подумал что лучше каждый раз спрашивать человека, продолжать или нет
    public void call(String s) {

        price.add(0);
        int sum = 0;
        for (Tariff f : ar) {
            sum += f.getPrice();
            System.out.println(f.getPrice());
            System.out.println(sum);
        }
        System.out.println("Calling " + s);
        System.out.println("You're current tariffs are " + ar);
        int cnt = 1;
        System.out.println("Call in process...");
        while (getBalance() > 0) {
            System.out.println(cnt + " mins past");
            setBalance(getBalance() - Tariff.perMinute * cnt - sum);
            System.out.println("Your current balance is " + getBalance());
            System.out.println("Would you like to stop your call?\nPress Y or N");
            s = in.next().toUpperCase();
            if (s.charAt(0) == 'Y') {
                break;
            }
            cnt++;
        }
        if (getBalance() <= 0) {
            System.out.println("Call has ended with " + cnt + " mins");
            System.out.println("Аккаунт заблокирован");
            map.replace(number, true);
            System.out.println("Предлагаем Вам ввести новый номер для звонка и баланс!");
            Main.start();



        }

    }

    public void addTariff() {
        if (!map.get(number)) {
            String s = in.next().toUpperCase();
            System.out.println(s);
            for (Tariff t : Tariff.values()) {
                if (t.name().equals(s)) {
                    System.out.println(t.name());
                    if (ar.contains(t)) {
                        continue;
                    }
                    ar.add(Tariff.valueOf(s));
                    price.add(ar.get(counter).getPrice());
                    counter++;
                    System.out.println(ar);
                    System.out.println(price);
                    break;
                }
            }
            containig(s);
        } else {
            System.out.println("Аккаунт заблокирован. Тарифы недоступны");
            System.out.println("Предлагаем Вам ввести новый номер для звонка!");
            Main.start();

        }
    }

    public void deleteTariff() {
        if (ar.size() == 0) {
            System.out.println("Тарифов нет");
        }
        if (!map.get(number)) {
            String s = in.next().toUpperCase();
            System.out.println(s);
            for (Tariff t : Tariff.values()) {
                if (t.name().equals(s)) {
                    System.out.println(t.name());
                    ar.remove(t);
                    System.out.println(price);
                    for (int i = 0; i < price.size(); i++) {
                        if (price.get(i) == t.getPrice()) {
                            price.remove(i);
                            break;
                        }
                    }
                    counter--;
                    System.out.println("Успешно удаленено!");

                    break;
                }
            }
            containig(s);
        } else {
            System.out.println("Аккаунт заблокирован. Тарифы недоступны");
            System.out.println("Предлагаем Вам ввести новый номер для звонка!");
            Main.start();

        }
    }

    private void containig(String s) {
        if (!Arrays.toString(Tariff.values()).contains(s)) {
            System.out.println("Такого тарифа нет. Повторите");
        }
        System.out.println("Ввести еще тариф?\nВведите Y или N");
        String x = in.next().toUpperCase();
        if (x.charAt(0) == 'Y') {
            addTariff();
        }
    }


    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public HashMap<String, Boolean> getMap() {
        return map;
    }

    public void setMap(HashMap<String, Boolean> map) {
        this.map = map;
    }
}
