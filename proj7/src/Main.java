import java.util.Arrays;
import java.util.HashMap;
import java.util.Scanner;

public class Main {
    private static final Scanner in = new Scanner(System.in);

    public static void main(String[] args) {
        start();
    }
    public static void start() {
        System.out.println("Введите номер и баланс");
        String number = in.next();
        int balance = in.nextInt();
        Abonent abonent = new Abonent(number, balance);
        HashMap<String, Boolean> q = new HashMap<>();
        q.put(number, false);
        abonent.setMap(q);
        System.out.println("Возможные тарифы");
        System.out.println(Arrays.toString(Tariff.values()));
        System.out.println("Добавить тариф?");
        System.out.println("Введите Y или N");
        String s = in.next().toUpperCase();
        if (s.charAt(0) == 'Y') {
            System.out.println("Введите название тарифа");
            abonent.addTariff();
        }
        System.out.println("Хотите удалить тариф?");
        System.out.println("Введите Y или N");
        s = in.next().toUpperCase();
        if (s.charAt(0) == 'Y') {
            System.out.println("Вводите");
            abonent.deleteTariff();
        }
        System.out.println("Введите номер для звонка");
        abonent.call(in.next());
    }
}
