public enum StudentsList {
    ANTON (1) ,
    EGOR (2),
    NOMAD (4),
    PLOXO (2),
    DEATH (1),
    DISCOVERY (5);

    private final int course;
    StudentsList(int course) {
        this.course = course;
    }
    public int getCourse() {
        return course;
    }
}
